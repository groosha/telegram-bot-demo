import logging

from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from os import getenv
from sys import exit

logging.basicConfig(level=logging.INFO)

if not getenv("BOT_TOKEN"):
    exit("Error: no token provided. Terminated.")

bot = Bot(token=getenv("BOT_TOKEN"))
dp = Dispatcher(bot)


@dp.message_handler(commands='version')
async def cmd_version(message: types.Message):
    await message.answer("Версия бота: 2")


@dp.message_handler(commands='start')
async def cmd_start(message: types.Message):
    await message.reply("Привет! Узнать версию: /version")


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
